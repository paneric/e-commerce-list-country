<?php

declare(strict_types=1);

use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Middleware\UriMiddleware;
use Paneric\Session\SessionMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Validation\ValidationService;
use Psr\Container\ContainerInterface;

return [
    RouteMiddleware::class => static function (ContainerInterface $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },

    UriMiddleware::class => static function (ContainerInterface $container): UriMiddleware {
        return new UriMiddleware($container);
    },

    SessionMiddleware::class => static function (ContainerInterface $container): SessionMiddleware {
        return new SessionMiddleware($container);
    },

    AuthenticationMiddleware::class => static function (ContainerInterface $container): AuthenticationMiddleware {
        return new AuthenticationMiddleware($container->get(SessionInterface::class));
    },

    CSRFMiddleware::class => static function (ContainerInterface $container): CSRFMiddleware {
        $config = $container->get('csrf');

        return new CSRFMiddleware(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $config
        );
    },

    ValidationMiddleware::class => static function (ContainerInterface $container): ValidationMiddleware {
        return new ValidationMiddleware(
            $container->get(ValidationService::class),
        );
    },

    PaginationMiddleware::class => static function (ContainerInterface $container): PaginationMiddleware {
        return new PaginationMiddleware(
            $container
        );
    },
];
