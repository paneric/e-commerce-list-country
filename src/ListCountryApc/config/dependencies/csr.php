<?php

use ECommerce\ListCountry\ListCountryApc\config\ListCountryApcActionConfig;
use ECommerce\ListCountry\ListCountryApc\config\ListCountryApcControllerConfig;
use ECommerce\ListCountry\ListCountryApc\Controller\ListCountryApcController;
use Paneric\ComponentModule\Module\Action\Apc\CreateApcAction;
use Paneric\ComponentModule\Module\Action\Apc\CreateMultipleApcAction;
use Paneric\ComponentModule\Module\Action\Apc\DeleteApcAction;
use Paneric\ComponentModule\Module\Action\Apc\DeleteMultipleApcAction;
use Paneric\ComponentModule\Module\Action\Apc\GetAllApcAction;
use Paneric\ComponentModule\Module\Action\Apc\GetAllPaginatedApcAction;
use Paneric\ComponentModule\Module\Action\Apc\GetOneByIdApcAction;
use Paneric\ComponentModule\Module\Action\Apc\UpdateApcAction;
use Paneric\ComponentModule\Module\Action\Apc\UpdateMultipleApcAction;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [
    ListCountryApcController::class => static function(ContainerInterface $container): ListCountryApcController
    {
        return new ListCountryApcController(
            $container->get(Twig::class),
            $container->get(ListCountryApcControllerConfig::class),
        );
    },

    'list_country_create_apc_action' => static function (ContainerInterface $container): CreateApcAction
    {
        return new CreateApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_create_multiple_apc_action' => static function (ContainerInterface $container): CreateMultipleApcAction
    {
        return new CreateMultipleApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_delete_apc_action' => static function (ContainerInterface $container): DeleteApcAction
    {
        return new DeleteApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_delete_multiple_apc_action' => static function (ContainerInterface $container): DeleteMultipleApcAction
    {
        return new DeleteMultipleApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_get_all_apc_action' => static function (ContainerInterface $container): GetAllApcAction
    {
        return new GetAllApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_get_all_paginated_apc_action' => static function (ContainerInterface $container): GetAllPaginatedApcAction
    {
        return new GetAllPaginatedApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_get_one_by_id_apc_action' => static function (ContainerInterface $container): GetOneByIdApcAction
    {
        return new GetOneByIdApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_update_apc_action' => static function (ContainerInterface $container): UpdateApcAction
    {
        return new UpdateApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },

    'list_country_update_multiple_apc_action' => static function (ContainerInterface $container): UpdateMultipleApcAction
    {
        return new UpdateMultipleApcAction (
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryApcActionConfig::class),
        );
    },
];
