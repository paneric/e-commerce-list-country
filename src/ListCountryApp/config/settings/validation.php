<?php

declare(strict_types=1);

use ECommerce\ListCountry\Gateway\ListCountryDTO;

return [

    'validation' => [

        'lc.add' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'lcs.add' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'lc.edit' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'lcs.edit' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'lc.remove' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'lcs.remove' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
