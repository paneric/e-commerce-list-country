<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApc\config;

use Paneric\Interfaces\Config\ConfigInterface;
use ECommerce\ListCountry\Gateway\ListCountryDAO;

class ListCountryApcActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        $apiEndpoints = [
            'base_url' => $_ENV['BASE_API_URL'],

            'api-prefix.get'       => '/api-lc/get/',
            'api-prefixs.get'      => '/api-lcs/get',
            'api-prefixs.get.page' => '/api-lcs/get/',

            'api-prefix.create'    => '/api-lc/create',
            'api-prefixs.create'   => '/api-lcs/create',

            'api-prefix.update'    => '/api-lc/update/',
            'api-prefixs.update'   => '/api-lcs/update',

            'api-prefix.delete'    => '/api-lc/delete/',
            'api-prefixs.delete'   => '/api-lcs/delete',

            'apc-prefixs.show-all-paginated' => '/apc-lcs/show-all-paginated/',
            'apc-prefixs.edit'               => '/apc-lcs/edit/',
            'apc-prefixs.remove'             => '/apc-lcs/remove/',
        ];

        return [
            'get_one_by_id' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'prefix' => 'lc'
                ]
            ),

            'get_all' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'prefix' => 'lc'
                ]
            ),

            'get_all_paginated' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'prefix' => 'lc'
                ]
            ),


            'create' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'prefix' => 'lc'
                ]
            ),

            'create_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'dao_class' => ListCountryDAO::class,
                    'prefix' => 'lc'
                ]
            ),

            'update' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                    'prefix' => 'lc'
                ]
            ),

            'update_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                ]
            ),

            'delete' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                ]
            ),

            'delete_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_country',
                ]
            ),
        ];
    }
}
