<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\config;

use ECommerce\ListCountry\Gateway\ListCountryDAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class ListCountryRepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => 'list_countrys',
            'dao_class' => ListCountryDAO::class,
            'fetch_mode' => PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
            'create_unique_where' => sprintf(
                ' %s',
                'WHERE lc_ref=:lc_ref'
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
                'WHERE lc_ref=:lc_ref',
                'AND lc_id NOT IN (:lc_id)'
            ),
        ];
    }
}
