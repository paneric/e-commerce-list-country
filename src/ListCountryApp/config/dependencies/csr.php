<?php

use ECommerce\ListCountry\ListCountryApp\config\ListCountryAppActionConfig;
use ECommerce\ListCountry\ListCountryApp\config\ListCountryAppControllerConfig;
use ECommerce\ListCountry\ListCountryApp\Controller\ListCountryAppController;
use ECommerce\ListCountry\Repository\ListCountryRepositoryInterface;
use Paneric\ComponentModule\Module\Action\App\CreateAppAction;
use Paneric\ComponentModule\Module\Action\App\CreateMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllPaginatedAppAction;
use Paneric\ComponentModule\Module\Action\App\GetOneByIdAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateMultipleAppAction;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [

    ListCountryAppController::class => static function(ContainerInterface $container): ListCountryAppController
    {
        return new ListCountryAppController(
            $container->get(Twig::class),
            $container->get(ListCountryAppControllerConfig::class),
        );
    },

    'list_country_create_app_action' => static function (ContainerInterface $container): CreateAppAction
    {
        return new CreateAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_create_multiple_app_action' => static function (ContainerInterface $container): CreateMultipleAppAction
    {
        return new CreateMultipleAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_delete_app_action' => static function (ContainerInterface $container): DeleteAppAction
    {
        return new DeleteAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_delete_multiple_app_action' => static function (ContainerInterface $container): DeleteMultipleAppAction
    {
        return new DeleteMultipleAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_get_all_app_action' => static function (ContainerInterface $container): GetAllAppAction
    {
        return new GetAllAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_get_all_paginated_app_action' => static function (ContainerInterface $container): GetAllPaginatedAppAction
    {
        return new GetAllPaginatedAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_get_one_by_id_app_action' => static function (ContainerInterface $container): GetOneByIdAppAction
    {
        return new GetOneByIdAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_update_app_action' => static function (ContainerInterface $container): UpdateAppAction
    {
        return new UpdateAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },

    'list_country_update_multiple_app_action' => static function (ContainerInterface $container): UpdateMultipleAppAction
    {
        return new UpdateMultipleAppAction (
            $container->get(ListCountryRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListCountryAppActionConfig::class),
        );
    },
];
