<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\Repository;

use Paneric\ComponentModule\Interfaces\Repository\ModuleRepositoryInterface;

interface ListCountryRepositoryInterface extends ModuleRepositoryInterface
{}
