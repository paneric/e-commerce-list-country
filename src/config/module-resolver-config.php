<?php

declare(strict_types=1);

return [
    'default_route_key' => 'lc',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'module_map' => [
        'lc'      => ROOT_FOLDER . 'src/ListCountryApp',
        'lcs'     => ROOT_FOLDER . 'src/ListCountryApp',
        'api-lc'  => ROOT_FOLDER . 'src/ListCountryApi',
        'api-lcs' => ROOT_FOLDER . 'src/ListCountryApi',
        'apc-lc'  => ROOT_FOLDER . 'src/ListCountryApc',
        'apc-lcs' => ROOT_FOLDER . 'src/ListCountryApc',
    ],
    'module_map_cross' => [
        'lc'       => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApp',
        'lcs'      => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApp',
        'api-lc'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApi',
        'api-lcs'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApi',
        'apc-lc'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApc',
        'apc-lcs'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-country/src/ListCountryApc',
    ],
];
