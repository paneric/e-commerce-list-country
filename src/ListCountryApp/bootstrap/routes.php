<?php

declare(strict_types=1);

use ECommerce\ListCountry\ListCountryApp\Controller\ListCountryAppController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->map(['GET'], '/lc/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->showOneById(
            $response,
            $this->get('list_country_get_one_by_id_app_action'),
            $args['id'] ?? '1'
        );
    })->setName('lc.show-one-by-id');


    $app->map(['GET'], '/lcs/show-all', function(Request $request, Response $response) {
        return $this->get(ListCountryAppController::class)->showAll(
            $response,
            $this->get('list_country_get_all_app_action')
        );
    })->setName('lcs.show-all');

    $app->map(['GET'], '/lcs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('lcs.show-all-paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/lc/add', function(Request $request, Response $response) {
        return $this->get(ListCountryAppController::class)->add(
            $request,
            $response,
            $this->get('list_country_create_app_action')
        );
    })->setName('lc.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/lcs/add', function(Request $request, Response $response) {
        return $this->get(ListCountryAppController::class)->addMultiple(
            $request,
            $response,
            $this->get('list_country_create_multiple_app_action')
        );
    })->setName('lcs.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));


    $app->map(['GET', 'POST'], '/lc/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->edit(
            $request,
            $response,
            $this->get('list_country_get_one_by_id_app_action'),
            $this->get('list_country_update_app_action'),
            $args['id']
        );
    })->setName('lc.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/lcs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->editMultiple(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_app_action'),
            $this->get('list_country_update_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('lcs.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/lc/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->remove(
            $request,
            $response,
            $this->get('list_country_delete_app_action'),
            $args['id']
        );
    })->setName('lc.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/lcs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryAppController::class)->removeMultiple(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_app_action'),
            $this->get('list_country_delete_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('lcs.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
