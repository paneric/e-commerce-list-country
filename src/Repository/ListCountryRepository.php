<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\Repository;

use Paneric\ComponentModule\Module\Repository\ModuleRepository;

class ListCountryRepository extends ModuleRepository implements ListCountryRepositoryInterface {}
