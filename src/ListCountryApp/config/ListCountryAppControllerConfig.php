<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApp\config;

use Paneric\Interfaces\Config\ConfigInterface;

class ListCountryAppControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'lc'
        ];
    }
}
