<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApp\config;

use ECommerce\ListCountry\Gateway\ListCountryDAO;
use ECommerce\ListCountry\Gateway\ListCountryDTO;
use Paneric\Interfaces\Config\ConfigInterface;

class ListCountryAppActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['lc_id' => (int) $id];
                },
            ],

            'get_all' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ],

            'create' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'dao_class' => ListCountryDAO::class,
                'dto_class' => ListCountryDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['lc_ref' => $attributes['ref']];
                },
            ],

            'create_multiple' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'dao_class' => ListCountryDAO::class,
                'dto_class' => ListCountryDTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = ['lc_ref' => $dao->getRef()];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'dao_class' => ListCountryDAO::class,
                'dto_class' => ListCountryDTO::class,
                'find_one_by_criteria' => static function (ListCountryDAO $dao, string $id): array
                {
                    return ['lc_id' => (int) $id, 'lc_ref' => $dao->getRef()];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['lc_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'module_name_sc' => 'list_country',
                'prefix' => 'lc',
                'dao_class' => ListCountryDAO::class,
                'dto_class' => ListCountryDTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            'lc_id' => (int) $index,
                            'lc_ref' => $dao->getRef(),
                        ];
                    }

                    return $findByCriteria;
                },

                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            'lc_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'module_name_sc' => 'list_country',
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['lc_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'module_name_sc' => 'list_country',
                'dao_class' => ListCountryDAO::class,
                'dto_class' => ListCountryDTO::class,
                'delete_by_criteria' => static function (array $daoCollection): array
                {
                    $deleteByCriteria = [];

                    foreach ($daoCollection as $index => $dao) {
                            $deleteByCriteria[$index]['lc_id'] = (int) $dao->getId();

                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
