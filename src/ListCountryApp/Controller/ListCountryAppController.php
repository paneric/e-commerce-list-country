<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApp\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleAppController;

class ListCountryAppController extends BaseModuleAppController {}
