<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\Gateway;

use Paneric\DataObject\DAO;

class ListCountryDAO extends DAO
{
    protected $id;
        protected $ref;//string 
    protected $pl;//string 
    protected $en;//string 

    public function __construct()
{
    $this->prefix = 'lc_';

    $this->setMaps();
}

    /**
     * @return null|int|string
     */
    public function getId()
{
    return $this->id;
}
        public function getRef(): ?string
    {
        return $this->ref;
    } 
    public function getPl(): ?string
    {
        return $this->pl;
    } 
    public function getEn(): ?string
    {
        return $this->en;
    } 

    /**
     * @var int|string
     */
    public function setId($id): void
{
    $this->id = $id;
}
        public function setRef(String $ref): void
    {
        $this->ref = $ref;
    } 
    public function setPl(String $pl): void
    {
        $this->pl = $pl;
    } 
    public function setEn(String $en): void
    {
        $this->en = $en;
    } 

}
