<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApi\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApiController;

class ListCountryApiController extends BaseModuleApiController {}
