<?php

declare(strict_types=1);

use ECommerce\ListCountry\Gateway\ListCountryDTO;

return [

    'validation' => [

        'api-lc.create' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-lcs.create' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-lc.update' => [
            'methods' => ['PUT'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-lcs.update' => [
            'methods' => ['PUT'],
            ListCountryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-lcs.delete' => [
            'methods' => ['POST'],
            ListCountryDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ]
                ],
            ],
        ],
    ]
];
