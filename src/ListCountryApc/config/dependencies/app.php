<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;

return [
    App::class => static function (ContainerInterface $container) {
        return AppFactory::createFromContainer($container);
    },

    'base_path' => static function (ContainerInterface $container) {
        $app = $container->get(App::class);

        return $app->getBasePath();
    },

    'route_parser_interface' => static function (ContainerInterface $container) {
        $app = $container->get(App::class);

        return $app->getRouteCollector()->getRouteParser();
    },
];
