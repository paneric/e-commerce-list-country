<?php

declare(strict_types=1);

use ECommerce\ListCountry\ListCountryApc\Controller\ListCountryApcController;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;

if (isset($app, $container)) {

    $app->map(['GET'], '/apc-lc/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->showOneById(
            $request,
            $response,
            $this->get('list_country_get_one_by_id_apc_action'),
            $args['id'] ?? '1'
        );
    })->setName('apc-lc.show-one-by-id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET'],'/apc-lcs/show-all', function(Request $request, Response $response) {
        return $this->get(ListCountryApcController::class)->showAll(
            $request,
            $response,
            $this->get('list_country_get_all_apc_action')
        );
    })->setName('apc-lcs.show-all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET'],'/apc-lcs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-lcs.show-all-paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!


    $app->map(['GET', 'POST'], '/apc-lc/add', function(Request $request, Response $response) {
        return $this->get(ListCountryApcController::class)->add(
            $request,
            $response,
            $this->get('list_country_create_apc_action')
        );
    })->setName('apc-lc.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-lcs/add', function(Request $request, Response $response) {
        return $this->get(ListCountryApcController::class)->addMultiple(
            $request,
            $response,
            $this->get('list_country_create_multiple_apc_action')
        );
    })->setName('apc-lcs.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-lc/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->edit(
            $request,
            $response,
            $this->get('list_country_get_one_by_id_apc_action'),
            $this->get('list_country_update_apc_action'),
            $args['id']
        );
    })->setName('apc-lc.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-lcs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->editMultiple(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_apc_action'),
            $this->get('list_country_update_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-lcs.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-lc/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->remove(
            $request,
            $response,
            $this->get('list_country_delete_apc_action'),
            $args['id']
        );
    })->setName('apc-lc.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-lcs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListCountryApcController::class)->removeMultiple(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_apc_action'),
            $this->get('list_country_delete_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-lcs.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
