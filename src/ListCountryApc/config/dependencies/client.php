<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Logger\LoggerFactory;
use Psr\Container\ContainerInterface;

return [
    Client::class => static function(ContainerInterface $container): Client
    {
        return new Client(
            $container->get('guzzle_client')
        );
    },
    HttpClientManager::class => static function(ContainerInterface $container): HttpClientManager
    {
        return new HttpClientManager(
            $container->get(Client::class),
            $container->get(LoggerFactory::class),
            [
                'filename' => 'http_client_manager.log',
                'name' => 'http_client_manager',
            ],
        );
    },
];
