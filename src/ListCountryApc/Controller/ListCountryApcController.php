<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApc\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApcController;

class ListCountryApcController extends BaseModuleApcController {}
