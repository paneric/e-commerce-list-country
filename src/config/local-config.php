<?php

declare(strict_types=1);

return [
    'dev' => [
        'name' => 'scm-lng',
        'default_local' => 'en',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => $_ENV['APP_DOMAIN'],
        'security' => false, //true if can be sent only by https
        'http_only' => true, //not accessible for javascript
    ],
    'test' => [
        'name' => 'scm-lng',
        'default_local' => 'en',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => $_ENV['APP_DOMAIN'],
        'security' => false, //true if can be sent only by https
        'http_only' => false, //not accessible for javascript
    ],
    'prod' => [
        'name' => 'scm-lng',
        'default_local' => 'en',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => $_ENV['APP_DOMAIN'],
        'security' => true, //true if can be sent only by https
        'http_only' => true, //not accessible for javascript
    ],
];
