<?php

declare(strict_types=1);

use ECommerce\ListCountry\ListCountryApi\Controller\ListCountryApiController;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->map(['GET'], '/api-lc/get/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListCountryApiController::class)->getOneById(
            $request,
            $response,
            $this->get('list_country_get_one_by_id_api_action'),
            $args['id']
        );
    })->setName('api-lc.get')
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->map(['GET'], '/api-lcs/get', function(Request $request, Response $response){
        return $this->get(ListCountryApiController::class)->getAll(
            $request,
            $response,
            $this->get('list_country_get_all_api_action')
        );
    })->setName('api-lcs.get')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->map(['GET'], '/api-lcs/get/{page}', function(Request $request, Response $response, array $args){
        return $this->get(ListCountryApiController::class)->getAllPaginated(
            $request,
            $response,
            $this->get('list_country_get_all_paginated_api_action'),
            $args['page']
        );
    })->setName('api-lcs.get.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->post('/api-lc/create', function(Request $request, Response $response){
        return $this->get(ListCountryApiController::class)->create(
            $request,
            $response,
            $this->get('list_country_create_api_action')
        );
    })->setName('api-lc.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-lcs/create', function(Request $request, Response $response){
        return $this->get(ListCountryApiController::class)->createMultiple(
            $request,
            $response,
            $this->get('list_country_create_multiple_api_action')
        );
    })->setName('api-lcs.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->put('/api-lc/update/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListCountryApiController::class)->update(
            $request,
            $response,
            $this->get('list_country_update_api_action'),
            $args['id']
        );
    })->setName('api-lc.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-lcs/update', function(Request $request, Response $response){
        return $this->get(ListCountryApiController::class)->updateMultiple(
            $request,
            $response,
            $this->get('list_country_update_multiple_api_action')
        );
    })->setName('api-lcs.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->delete('/api-lc/delete/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListCountryApiController::class)->delete(
            $request,
            $response,
            $this->get('list_country_delete_api_action'),
            $args['id']
        );
    })->setName('api-lc.delete')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-lcs/delete', function(Request $request, Response $response){
        return $this->get(ListCountryApiController::class)->deleteMultiple(
            $request,
            $response,
            $this->get('list_country_delete_multiple_api_action')
        );
    })->setName('api-lcs.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));
}
