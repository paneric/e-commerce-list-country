<?php

declare(strict_types=1);

use ECommerce\ListCountry\config\ListCountryRepositoryConfig;
use ECommerce\ListCountry\Repository\ListCountryRepository;
use ECommerce\ListCountry\Repository\ListCountryRepositoryInterface;
use Paneric\DBAL\Manager;
use Psr\Container\ContainerInterface;

return [
    ListCountryRepositoryInterface::class => static function (ContainerInterface $container): ListCountryRepository {
        return new ListCountryRepository(
            $container->get(Manager::class),
            $container->get(ListCountryRepositoryConfig::class)
        );
    },
];
