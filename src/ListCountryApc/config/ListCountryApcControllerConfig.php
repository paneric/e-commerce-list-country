<?php

declare(strict_types=1);

namespace ECommerce\ListCountry\ListCountryApc\config;

use Paneric\Interfaces\Config\ConfigInterface;

class ListCountryApcControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'lc'
        ];
    }
}
